let materia; // nombre de la materia
let codigo; //codigo del estudiante
let notas; //cantidad de notas a borrar 
let nombre; //nombre del estudiante
let tareas = []; /// este arreglo guarda la onfo de las notas
let tareas2 = []; /// este arreglo tiene la informacion de las notas en orden ascendente
let descripcion = [];
let laTarea = [];

// este método pasa por url los datos que se incluyeron en el form del index
function obtenerDatos() {
    let url = new URLSearchParams(location.search);
    codigo = url.get('codigo');
    notas = Number(url.get('notas'));
    buscarAlumno(codigo);
}

//este método busca la información del estudiante

function buscarAlumno(codigo) {
    fetch(`https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json`)
        .then(res => res.json())
        //.then(alumnos => console.log(alumnos))
        .then(alumnos => {
            materia = alumnos.nombreMateria; // guardo el nombre de la materia que es programacion web
            datosEstudiante(alumnos, codigo);// mando a buscar el nombre del estudiante
            document.getElementById("informacion").innerHTML = tablaDatos();
        })
}

//este metodo busca el nombre del estudiante y manda a guarda las notas
function datosEstudiante(datos, codigo) {
    let conseguido = false;
    for (let i = 0; i < datos.estudiantes.length; i++) {
        if (datos.estudiantes[i].codigo == codigo) {
            nombre = datos.estudiantes[i].nombre;
            sacarNotas(datos.estudiantes[i])/// le mando solo el estudiante i
            //console.log(tareas);
            console.log(tareas2);
            obtenerDescripciones(datos);
            //console.log(descripcion);
            eliminarNotas();
            describir();
            catalogar(datos);
            //console.log(laTarea);
            conseguido = true;
        }
    }
    if (conseguido == false) {
        alert("Código equivocado");
    }
}

//sacar las notas del estudiante y las ordena en el array tareas orden

function sacarNotas(estudiante) {
    for (let i = 0; i < 15; i++) {
        tareas.push(estudiante.notas[i].valor);
        tareas2.push(estudiante.notas[i]);

    }
    tareas.sort();
}



/// obtener descripciones
function obtenerDescripciones(datos) {
    for (let i = 0; i < 15; i++) {
        descripcion.push(datos.descripcion[i].descripcion);
    }
}

//eliminar notas

function eliminarNotas() {
    //el primer for recorre el []de las notas ordenadas
    for (let cant = 0; cant < notas; cant++) {
        //el segundo for busca la nota en el [] de las notas no ordenadas
        for (let i = 0; i < 15; i++) {
            /// cuando la encuentra marca esa casilla como elimonada en [] de las descripciones
            if (tareas[cant] == tareas2[i].valor) {
                descripcion[i] = "nota eliminada";
                break;
            }
        }
    }
}

//acomodar las descripciones
function describir() {
    for (let i = 0; i < 15; i++) {
        if (tareas2[i].valor >= 3.0 && descripcion[i] != "nota eliminada") {
            descripcion[i] = "nota aprobada";
        }
        if (tareas2[i].valor < 3.0 && descripcion[i] != "nota eliminada") {
            descripcion[i] = "nota reprobada";
        }
    }
}

function catalogar(info) {
    for (let i = 0; i < 15; i++) {
        laTarea.push(info.descripcion[i].descripcion);
    }
}


obtenerDatos();

google.charts.load('current', { 'packages': ['table'] });
google.charts.setOnLoadCallback(drawTable);

function drawTable() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descripción');
    data.addColumn('number', 'Valor');
    data.addColumn('string', 'Observacion');
    data.addRows(16);

    for (let i = 0; i < 15; i++) {
        data.setCell(i, 0, laTarea[i]);
        data.setCell(i, 1, tareas2[i].valor);
        data.setCell(i, 2, descripcion[i]);
    }

    var table = new google.visualization.Table(document.getElementById('tablaUno'));

    table.draw(data, { showRowNumber: false, width: '500%', height: '500%' });
}


//este metodo crea la tabla donde se pone la info del estudiante y la materia
function tablaDatos() {
    let fila1 = `<tr><td>Nombre: </td> <td> ${nombre}</td></tr>`;
    let fila2 = `<tr><td>Código: </td> <td> ${codigo}</td></tr>`;
    let fila3 = `<tr><td>Materia: </td> <td> ${materia} </td></tr>`;

    fila1 += fila2;
    fila1 += fila3;

    return fila1;

}
